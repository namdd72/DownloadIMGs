﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace DownloadIMGs.Command
{
    class Program
    {
        private static WebClient webClient = new WebClient()
        {
            BaseAddress = "https://www.instagram.com"
        };

        private static string getSource(string url)
        {
            string source = "";

            source = webClient.DownloadString(url);

            return source;
        }

        private static void downloadFileFrom(string source)
        {
            if (!Directory.Exists("./result"))
                Directory.CreateDirectory("./result");

            var shortCodes = Regex.Matches(source, "shortcode\":\"(?<shortCode>.*?)\"");

            foreach (Match shortCode in shortCodes)
            {
                var link = getLinkFrom(shortCode.Groups["shortCode"].Value);
                var fileName = link.Substring(link.LastIndexOf("/")).Split('?')[0];
                webClient.DownloadFile(link, "./result" + fileName);
            }
        }

        private static string getLinkFrom(string shortCode)
        {
            string source = getSource("p/" + shortCode);

            var link = Regex.Match(source, "property=\"og:image\" content=\"(?<link>.*)\"");

            return link.Groups["link"].Value;
        }

        static void Main(string[] args)
        {
            Console.Write("username: ");
            // Cô Ly: string url = "may__lily"; 
            string url = Console.ReadLine();

            Console.Write("Downloading . . .");

            string source = getSource(url);

            downloadFileFrom(source);

            Process.Start(@"explorer", Directory.GetCurrentDirectory() + @"\result");
        }
    }
}
