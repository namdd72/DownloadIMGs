﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DowloadIMGs.GUI
{
    public partial class Form1 : Form
    {
        public Form1() => InitializeComponent();

        private WebClient webClient = new WebClient()
        {
            BaseAddress = "https://www.instagram.com"
        };
        private List<Image> imgs = new List<Image>();

        private string getSource(string url)
        {
            string source = "";

            source = webClient.DownloadString(url);

            return source;
        }

        private void downloadFileFrom(string source)
        {
            if (!Directory.Exists("./result"))
                Directory.CreateDirectory("./result");

            var shortCodes = Regex.Matches(source, "shortcode\":\"(?<shortCode>.*?)\"");

            foreach (Match shortCode in shortCodes)
            {
                var link = getLinkFrom(shortCode.Groups["shortCode"].Value);
                var fileName = link.Substring(link.LastIndexOf("/")).Split('?')[0];

                var byteimg = webClient.DownloadData(link);
                var ms = new MemoryStream(byteimg, 0, byteimg.Length);
                var img = Image.FromStream(ms);

                imgs.Add(img);
                DGV_IMGs.Rows.Add(fileName);
            }
        }

        private string getLinkFrom(string shortCode)
        {
            string source = getSource("p/" + shortCode);

            var link = Regex.Match(source, "property=\"og:image\" content=\"(?<link>.*)\"");

            return link.Groups["link"].Value;
        }

        private void preview(Image image)
        {
            PB_Preview.Image = image;

            PB_Preview.Size = new Size()
            {
                Height = PB_Preview.Size.Height,
                Width = (int)(PB_Preview.Size.Height * image.Size.Width / (double)image.Size.Height)
            };
        }

        private void BT_Download_Click(object sender, EventArgs e)
        {
            PP_Progress.Visible = true;

            string url = TB_UserName.Text;

            string source = getSource(url);

            downloadFileFrom(source);

            PP_Progress.Visible = false;
        }

        private void DGV_IMGs_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            preview(imgs[e.RowIndex]);
        }
    }
}