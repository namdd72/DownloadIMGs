﻿namespace DowloadIMGs.GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TB_UserName = new System.Windows.Forms.TextBox();
            this.BT_Download = new System.Windows.Forms.Button();
            this.DGV_IMGs = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PB_Preview = new System.Windows.Forms.PictureBox();
            this.PP_Progress = new DevExpress.XtraWaitForm.ProgressPanel();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_IMGs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Preview)).BeginInit();
            this.SuspendLayout();
            // 
            // TB_UserName
            // 
            this.TB_UserName.Location = new System.Drawing.Point(40, 28);
            this.TB_UserName.Name = "TB_UserName";
            this.TB_UserName.Size = new System.Drawing.Size(192, 20);
            this.TB_UserName.TabIndex = 0;
            // 
            // BT_Download
            // 
            this.BT_Download.Location = new System.Drawing.Point(238, 27);
            this.BT_Download.Name = "BT_Download";
            this.BT_Download.Size = new System.Drawing.Size(75, 23);
            this.BT_Download.TabIndex = 1;
            this.BT_Download.Text = "Download";
            this.BT_Download.UseVisualStyleBackColor = true;
            this.BT_Download.Click += new System.EventHandler(this.BT_Download_Click);
            // 
            // DGV_IMGs
            // 
            this.DGV_IMGs.AllowUserToAddRows = false;
            this.DGV_IMGs.AllowUserToDeleteRows = false;
            this.DGV_IMGs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.DGV_IMGs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGV_IMGs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_IMGs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.DGV_IMGs.Location = new System.Drawing.Point(40, 78);
            this.DGV_IMGs.Name = "DGV_IMGs";
            this.DGV_IMGs.ReadOnly = true;
            this.DGV_IMGs.Size = new System.Drawing.Size(273, 224);
            this.DGV_IMGs.TabIndex = 2;
            this.DGV_IMGs.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_IMGs_CellClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "images";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // PB_Preview
            // 
            this.PB_Preview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PB_Preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PB_Preview.Location = new System.Drawing.Point(319, 26);
            this.PB_Preview.Name = "PB_Preview";
            this.PB_Preview.Size = new System.Drawing.Size(310, 276);
            this.PB_Preview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PB_Preview.TabIndex = 3;
            this.PB_Preview.TabStop = false;
            // 
            // PP_Progress
            // 
            this.PP_Progress.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PP_Progress.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.PP_Progress.Appearance.Options.UseBackColor = true;
            this.PP_Progress.BarAnimationElementThickness = 2;
            this.PP_Progress.Description = "Downloading . . .";
            this.PP_Progress.Location = new System.Drawing.Point(204, 140);
            this.PP_Progress.Name = "PP_Progress";
            this.PP_Progress.Size = new System.Drawing.Size(246, 66);
            this.PP_Progress.TabIndex = 4;
            this.PP_Progress.TabStop = false;
            this.PP_Progress.Text = "progressPanel1";
            this.PP_Progress.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 322);
            this.Controls.Add(this.PP_Progress);
            this.Controls.Add(this.PB_Preview);
            this.Controls.Add(this.DGV_IMGs);
            this.Controls.Add(this.BT_Download);
            this.Controls.Add(this.TB_UserName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.DGV_IMGs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB_Preview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TB_UserName;
        private System.Windows.Forms.Button BT_Download;
        private System.Windows.Forms.DataGridView DGV_IMGs;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.PictureBox PB_Preview;
        private DevExpress.XtraWaitForm.ProgressPanel PP_Progress;
    }
}

